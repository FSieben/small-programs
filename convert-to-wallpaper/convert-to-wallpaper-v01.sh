#!/usr/bin/env sh

i=1
resolution="2560x1440"
color="black"
output_file="output"

for file in *
do

    filename=$(basename "$file")
    extension=${filename##*.}

    if [ $extension == "jpg" ] || [ $extension == "webp" ]
    then
        echo Old filename: $filename

        new_filename=$(printf "$output_file-%02d.jpg" $i)
        echo New filename: $new_filename

        command="-resize $resolution -background $color -extent $resolution -gravity center $filename  $new_filename"
        convert $command
        i=$(($i+1))

    fi

done
