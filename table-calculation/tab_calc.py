#!venv/bin/python

import pandas as pd
import matplotlib.pyplot as plt
import sys, getopt


def usage():
    """Print help for the command-line arguments and options."""
    print("""tab_calc.py -i <inputfile>""")


def get_arguments(argv):
    """Get program arguments and handle this."""
    inputfile = ""
    plottable = [False, 0]
    drawtable = [False, 0]

    # Get arguments. If one option not given raise an error.
    # Help getopt(): https://docs.python.org/3/library/getopt.html
    try:
        opts, args = getopt.getopt(argv, "hi:p:t:")
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    # If no option is entered, print help.
    if len(opts) > 0:
        for opt, arg in opts:
            if opt == "-h":
                usage()
                sys.exit(1)
            elif opt == "-i":
                inputfile = [True, arg]
            elif opt == "-p":
                plottable = [True, int(arg)]
            elif opt == "-t":
                drawtable = [True, int(arg)]
        return {"Input-file": inputfile,
                "Plot-Table": plottable,
                "Draw-Table": drawtable}
    else:
        # If no option is given.
        usage()
        sys.exit(1)


def process_file(option):
    """Process a csv file."""
    # Read csv file
    table = pd.read_csv(option["Input-file"][1], sep="\t")
    # print(pd.to_datetime(table['Date']))
    table['Date'] = pd.to_datetime(table['Date'])

    # Calculate sum of each row (for each date)
    # New panda series with the sum
    # table.iloc[:, 1:] = All rows, columen 1 to n (begining counting by 0!)
    sum_calc = table.iloc[:, 1:].sum(axis=1)
    calc = pd.DataFrame({'Sum': sum_calc,
                         'Diff': sum_calc.diff(periods=1),
                         'Per': sum_calc.pct_change(periods=1)*100,
                         'Per(12)': sum_calc.pct_change(periods=12)*100})

    return table, calc


def output_table(option, df_org, df_process):
    """Print or plot table."""
    df_table = pd.concat([df_org, df_process], axis=1)
    # print(df_table)
    if option['Draw-Table'][0]:
        if option['Draw-Table'][1] > 0:
            print(df_table.tail(n=option['Draw-Table'][1]).to_markdown())
        else:
            print(df_table.to_markdown())
    elif option['Plot-Table'][0]:
        if option['Plot-Table'][1] > 0:
            print("Plot n>0")
            df_table.plot(x='Date', y=df_table.columns[option['Plot-Table'][1]])
        else:
            df_table.plot(x='Date', y='Sum')
        plt.grid()
        plt.show()


if __name__ == "__main__":
    """Start program"""
    dict_args = get_arguments(sys.argv[1:])

    # Process the table
    df_org, df_process = process_file(dict_args)

    # Output the table
    output_table(dict_args, df_org, df_process)
